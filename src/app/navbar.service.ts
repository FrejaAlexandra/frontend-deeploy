export class NavbarService {
  getNavbar() {
    //return ["Predictions", "Performance"];
    return [{href: "/predictions", label: "Predictions"}, {href: "/performance", label: "Performance"}]
  }
}
