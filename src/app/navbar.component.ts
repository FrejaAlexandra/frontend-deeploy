import { Component } from '@angular/core';
import { NavbarService } from './navbar.service';


@Component({
  selector: 'navbar',
  template: `
    <ul>
      <li class="nav-items" *ngFor="let nav of navbar">
        <a href="{{ nav.href }}" class="nav-items">{{ nav.label }}</a>
      </li>
    </ul>
    `
})

export class NavbarComponent {
  title = "Navbar"
  navbar;

  constructor(service: NavbarService) {
    this.navbar = service.getNavbar();
  }
}
